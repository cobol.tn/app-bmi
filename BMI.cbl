       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. Holmes

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WEIGHT      PIC   999v9    VALUE ZEROS .
       01  HEIGHT      PIC   999v9     VALUE ZERO.
       01  BMI         PIC   99v9    VALUE ZEROS .

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter weigsh(kg.) : - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Enter Heigsh(cm.) : - " WITH NO ADVANCING
           ACCEPT HEIGHT
           COMPUTE BMI = WEIGHT / (HEIGHT / 100)**2

           DISPLAY "BMI is "BMI WITH NO ADVANCING 

           EVALUATE TRUE
              WHEN BMI  > 30
                 DISPLAY ' Class 3 Obesity'
              WHEN BMI  > 24.9
                 DISPLAY ' Obesity'
              WHEN BMI  > 22.9
                 DISPLAY ' Overweight'
              WHEN BMI  > 18.5
                 DISPLAY ' Healthy weight'
              WHEN OTHER
                 DISPLAY ' Underweight'
           END-EVALUATE

           GOBACK.
   